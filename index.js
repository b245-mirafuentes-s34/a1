const express = require('express')
const port = 4000

const app = express()

app.use(express.json())

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];

app.get('/home', (request, response) =>{
	response.send("Welcome to home page")
})

app.get('/items', (request, response) =>{
	response.send(items)
})

app.delete('/delete-item', (request, response) =>{
	response.send(items.pop())
})



app.listen(port, console.log("Server is running at port", port))